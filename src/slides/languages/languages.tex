\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

\usepackage{../presentation,../presentationfonts}
\usepackage{../../grammar}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Languages}

\author{Jim Hef{}feron}
\institute{
  Mathematics and Statistics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Cardinality}

\usepackage{catchfilebetweentags} % read text from background.tex
\newcommand{\catchfilefn}{../../languages/languages.tex}
% Keep from swallowing end-of-lines
%   https://tex.stackexchange.com/a/40704/121234
\usepackage{etoolbox}
\makeatletter
\patchcmd{\CatchFBT@Fin@l}{\endlinechar\m@ne}{}
  {}{\typeout{Unsuccessful patch!}}
\makeatother

\usepackage{xr}
  \externaldocument{../../book.aux}
\usepackage{cleveref}

% \usepackage{multirow,bigstrut}
\begin{document}
\begin{frame}
  \titlepage
\end{frame}




\section{Strings}
% ------------------------------------------------------
\begin{frame}
  \frametitle{Definitions}
Recall that a \definend{symbol} (sometimes called a \definend{token}) 
is something that a machine can read and write,
and an \definend{alphabet} is a nonempty and finite set of symbols.

\ExecuteMetaData[\catchfilefn]{def:String}

We write symbols in a distinct typeface, as in \str{1} or \str{a},
because the alternative of quoting them would be 
clunky.

\ExecuteMetaData[\catchfilefn]{discussion:AlphSymbol}.
\ExecuteMetaData[\catchfilefn]{def:StringLength}
\ExecuteMetaData[\catchfilefn]{dis:OmitCommas}

\begin{example}
Natural numbers are represented by strings of digits.
The symbols are \str{0}, \str{1}, \ldots{}
and the alphabet is $\Sigma=\set{\str{0},\ldots \str{9}}$.
The string $\sigma=\str{1066}$ has length~$|\sigma|=4$. 
\end{example}

\ExecuteMetaData[\catchfilefn]{def:Bitstrings}
\end{frame}




\begin{frame}[fragile]
  \frametitle{Languages}
\ExecuteMetaData[\catchfilefn]{def:Kleenestar}

\begin{example}
Where $\Sigma=\B$, the set of bitstrings of length~$3$ is
$\B^3=\set{\str{000},\str{001}, \ldots \str{111}}$.
The set of all bitstrings is 
$\kleenestar{\B}=\set{\emptystring,\str{0},\str{1},\str{00},\ldots}$.
\end{example}

\ExecuteMetaData[\catchfilefn]{def:StringOps}

\ExecuteMetaData[\catchfilefn]{def:Lang}

\ExecuteMetaData[\catchfilefn]{def:class}
\end{frame}



\begin{frame}
\begin{example}
Let $\Sigma=\set{\str{a},\str{b}}$.
One language is
$\lang_0=\set{\sigma\in\kleenestar{\Sigma}\suchthat\text{$\sigma=\str{a}^n\str{b}\str{a}^n$ for some $n\in\N$}}$; some members are 
\str{b}, \str{aba}, \str{aabaa}, and \str{aaabaaa}.
Another is the language of \definend{palindromes}
$\lang_1=\set{\sigma\in\kleenestar{\Sigma}\suchthat \sigma=\reversal{\sigma}}$.
\end{example}

\begin{example}
For $\Sigma=\set{\str{)}, \str{(}}$ the set of strings of balanced 
parentheses is a language.
Some members are \str{(())}, \str{(()())}, and \str{(()(()))}.
Nonmembers: \str{)(}, \str{(()}, and \str{())(()}. 
\end{example}

\ExecuteMetaData[\catchfilefn]{def:LangOps}

\ExecuteMetaData[\catchfilefn]{ex:KleeneStar}
\end{frame}





% ==============================================
\section{Grammars}

\begin{frame}
  \frametitle{Some rules for English}

\begin{itemize}
\ExecuteMetaData[\catchfilefn]{ex:RulesForEnglish} 
\end{itemize}
We will write them this way.
\ExecuteMetaData[\catchfilefn]{ex:GrammarRulesForEnglish} 
\end{frame}


\begin{frame}
  \frametitle{Vocabulary}
\ExecuteMetaData[\catchfilefn]{discussion:GrammarVocabulary} 
\end{frame}



\begin{frame}
  \frametitle{Derivation}
\ExecuteMetaData[\catchfilefn]{discussion:DerivationVocabulary} 
\end{frame}


\begin{frame}
  \frametitle{Derivation tree}
\ExecuteMetaData[\catchfilefn]{discussion:DerivationTree} 
\begin{center}
  \includegraphics{../../languages/asy/parsetree/parsetree000.pdf}
\end{center}
\end{frame}


\begin{frame}
  \frametitle{Grammar}
\ExecuteMetaData[\catchfilefn]{def:Grammar} 

\begin{example}
Let the alphabet be $\Sigma=\set{\trm{a},\trm{b}}$,
let the set of nonterminals be $N=\set{\ntrm{start},\ntrm{left},\ntrm{right}}$,
and let the start symbol be \ntrm{start}.
Here are the five production rules in the set~$P$.
\begin{grammar}
\ntrm{start} \produces \trm{a}\ntrm{left}

\ntrm{left} \produces \trm{a}\ntrm{left} 
  \syntaxor \trm{b}\ntrm{right}

\ntrm{right} \produces \trm{b}\ntrm{right} \syntaxor \emptystring
\end{grammar}
\end{example}

We will usually not explicitly name the start symbol, but instead
take the convention that it is the head of the first rule.
Here is a derivation using the production rules.
\begin{center}
\ntrm{start}\derives \trm{a}\ntrm{left} \derives \trm{aa}\ntrm{left}
  \derives \trm{aab}\ntrm{right} \derives \trm{aabb}\ntrm{right} 
  \derives \trm{aabb}\emptystring = \trm{aabb}
\end{center}
\end{frame}

\begin{frame}
\begin{example}
This is part of the grammar of C.

\begin{grammar}
\ntrm{statement\_list}
	\produces{} \ntrm{statement}
	\syntaxor \ntrm{statement\_list} \ntrm{statement}

\ntrm{expression\_statement}
	\produces{} \trm{;}
	\syntaxor \ntrm{expression} \ntrm{;}

\ntrm{selection\_statement}
	\produces IF \trm{(} \ntrm{expression} \trm{)} \ntrm{statement}
	\othersyntax IF \trm{(} \ntrm{expression} \trm{)} \ntrm{statement} ELSE \ntrm{statement}
	\othersyntax SWITCH \trm{(} \ntrm{expression} \trm{)} \ntrm{statement}

\ntrm{iteration\_statement}
	\produces WHILE \trm{(} \ntrm{expression} \trm{)} \ntrm{statement}
	\othersyntax DO \ntrm{statement} WHILE \trm{(} \ntrm{expression} \trm{)} \trm{;}
	\othersyntax FOR \trm{(} \ntrm{expression\_statement} \ntrm{expression\_statement} \trm{)} \ntrm{statement}
	\othersyntax FOR \trm{(} \ntrm{expression\_statement} \ntrm{expression\_statement} \ntrm{expression} \trm{)} \ntrm{statement}
\end{grammar}
As to the capitalization: `IF' essentially 
means `any string that capitalizes to IF'.
\end{example}
\end{frame}


\begin{frame}
  \frametitle{Recursive grammars}

The two example grammars are recursive. 
For instance, \ntrm{left} can expand to
an expression involving \ntrm{left}.
\begin{grammar}
\ntrm{left} \produces \trm{a}\ntrm{left} 
  \othersyntax \trm{b}\ntrm{right}
\end{grammar}

Similarly, \ntrm{statement\_list}'s expansion involves itself.
\begin{grammar}
\ntrm{statement\_list}
	\produces{} \ntrm{statement}
	\syntaxor \ntrm{statement\_list} \ntrm{statement}
\end{grammar}
\end{frame}


\begin{frame}
  \frametitle{Abbreviation}

For toy examples and exercises we may use capital letters as nonterminals
and lowercase letters or digits as terminals.

\begin{example}
The strings derived from this grammar are elementary 
arithemtic expressions 
\begin{grammar}
S \produces \trm{(} S \trm{)} \syntaxor S \trm{+} S \syntaxor S \trm{*} S \syntaxor I

I \produces{} \trm{0} \syntaxor \trm{1} \syntaxor \ldots{} \trm{9}
\end{grammar}
such as \str{1+2*(3+4)}.
\end{example}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Ambiguity of a grammar}
% \ExecuteMetaData[\catchfilefn]{rem:AmbiguousGrammar} 
This bit of C language-like code shows nested
\codeinline{if} statements. 
\begin{lstlisting}
  if ... then ... if ... then ... else ...
\end{lstlisting}
This is a \definend{dangling else}.
The \codeinline{else} could be associated
with the first \codeinline{if} or the second.
\begin{center}
  \begin{minipage}{0.4\linewidth}
    \begin{lstlisting}
 if ... 
 then ...
     if ...
     then ... 
 else ...
\end{lstlisting}
  \end{minipage}
  \quad
  \begin{minipage}{0.4\linewidth}
    \begin{lstlisting}
 if ... 
 then ...
     if ...
     then ... 
     else ...
\end{lstlisting}
  \end{minipage}
\end{center}
An \definend{ambiguous grammar}
is one where some strings
have two different leftmost derivations. 
This grammar
\begin{grammar}
\ntrm{expr} \produces{} \ntrm{expr} \trm{+} \ntrm{expr}
        \othersyntax  \ntrm{expr} \trm{*} \ntrm{expr>}
        \othersyntax  \trm{(} \ntrm{expr} \trm{)} \
               \syntaxor  \trm{a}  \syntaxor  \trm{b}  \syntaxor \ldots{} 
               \trm{z}
\end{grammar}
is ambiguous because \str{a+b*c} has two
leftmost derivations.
\begin{center}
\hspace{1em}\begin{tabular}{l}
  \ntrm{expr}\derives\ntrm{expr}\trm{+}\ntrm{expr}
  \derives\trm{a} \trm{+} \ntrm{expr}                 \\ \quad
  \derives\trm{a} \trm{+} \ntrm{expr} \trm{*} \ntrm{expr}          
  \derives\trm{a} \trm{+} \trm{b} \trm{*} \ntrm{expr}          
  \derives\trm{a} \trm{+} \trm{b} \trm{*} \trm{c}          
\end{tabular}        \\[1ex]
\hspace{1em}\begin{tabular}{l}
  \ntrm{expr}\derives\ntrm{expr} \trm{*} \ntrm{expr}
  \derives\ntrm{expr} \trm{+} \ntrm{expr} \trm{*} \ntrm{expr}    \\ \quad 
  \derives\trm{a} \trm{+} \ntrm{expr} \trm{*} \ntrm{expr}  
  \derives\trm{a} \trm{+} \trm{b} \trm{*} \ntrm{expr}  
  \derives\trm{a} \trm{+} \trm{b} \trm{*} \trm{c}          
\end{tabular}
\end{center}
\end{frame}


\begin{frame}
  \frametitle{Language of a grammar}
\ExecuteMetaData[\catchfilefn]{def:LanguageOfGrammar} 

\begin{example}
This grammar~$G$
\begin{grammar}
S \produces AB

A \produces aA \syntaxor a

B\produces bB \syntaxor b
\end{grammar}
lets you derive strings consisting of some \str{a}'s folowed by some \str{b}'s.
\begin{equation*}
\lang(G)=
\set{\str{a}^n\str{b}^m\suchthat n,m\in\N}
\end{equation*}
\end{example}
\end{frame}





% =================================================================
\section{BNF}
\begin{frame}[fragile]{Backus-Naur Form notation}
Many programming languages, protocols, or formats use Backus-Naur form 
description for their specification.
Every rule has the structure.
\mbox{\ntrm{name} \bnfproduces \ntrm{expansion}}.
For instance, this is a grammar of arithmetic expressions.
\begin{grammar}
\ntrm{expr} \bnfproduces \ntrm{term} \trm{+} \ntrm{expr}
         \othersyntax  \ntrm{term}

\ntrm{term} \bnfproduces \ntrm{factor} \trm{*} \ntrm{term}
         \othersyntax  \ntrm{factor}

\ntrm{factor} \bnfproduces \trm{(} \ntrm{expr} \trm{)}
           \othersyntax  \ntrm{const}

\ntrm{const} \bnfproduces \trm{0} \syntaxor \trm{1} \syntaxor \ldots{} \trm{9} \syntaxor \ntrm{constant}\ntrm{constant}
\end{grammar}

\pause
In a specification it may look like this.
\begin{lstlisting}
<expr> ::= <term> "+" <expr>
         |  <term>

<term> ::= <factor> "*" <term>
         |  <factor>

<factor> ::= "(" <expr> ")"
           |  <const>

<const> ::= integer
\end{lstlisting}
\end{frame}

\begin{frame}{Common extensions to BNF}
\begin{description}
\item[Repetition] To show the element may be repeated zero or more times,
use curly braces.
This is a comma-separated argument list.
\begin{grammar}
\ntrm{args} \bnfproduces \ntrm{arg} \{ \trm{,} \ntrm{arg} \trm{} \}
\end{grammar}
To denote zero or more times you may also see a Kleene star,~$*$, and
to denote one or more times you may see a plus,~$+$.

For zero or one repetitions, you may see square brackets.
\begin{grammar}
\ntrm{int} \bnfproduces [ \trm{+} \syntaxor \trm{-} ] \ntrm{natural}
\end{grammar}

\pause\item[Grouping] Use parentheses. 
This shows two terms being either added or subtracted.
\begin{grammar}
\ntrm{expr} \bnfproduces \ntrm{term} ( \trm{+} | \trm{-} ) \ntrm{expr}
\end{grammar}
In addition to the vertical bar for ``or'', the parentheses are metacharacters.

\pause\item[Concatenation]
Rather than relying on juxtaposition (which is invisible), a comma~\trm{,} 
may explicitly denote concatenation.
\end{description}
\end{frame}


\begin{frame}[fragile]
Here is a definition of a date and time format taken from RFC~5322.
\begin{lstlisting}
date-time       =   [ day-of-week "," ] date time [CFWS]

   day-of-week     =   ([FWS] day-name) / obs-day-of-week

   day-name        =   "Mon" / "Tue" / "Wed" / "Thu" /
                       "Fri" / "Sat" / "Sun"

   date            =   day month year

   day             =   ([FWS] 1*2DIGIT FWS) / obs-day

   month           =   "Jan" / "Feb" / "Mar" / "Apr" /
                       "May" / "Jun" / "Jul" / "Aug" /
                       "Sep" / "Oct" / "Nov" / "Dec"

   year            =   (FWS 4*DIGIT FWS) / obs-year

   time            =   time-of-day zone

   time-of-day     =   hour ":" minute [ ":" second ]

   hour            =   2DIGIT / obs-hour

   minute          =   2DIGIT / obs-minute

   second          =   2DIGIT / obs-second

   zone            =   (FWS ( "+" / "-" ) 4DIGIT) / obs-zone
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Python's floating point}
This is the grammar for floating point numbers in Python.
\begin{lstlisting}
floatnumber   ::=  pointfloat | exponentfloat

pointfloat    ::=  [intpart] fraction | intpart "."

exponentfloat ::=  (intpart | pointfloat) exponent

intpart       ::=  digit+

fraction      ::=  "." digit+

exponent      ::=  ("e" | "E") ["+" | "-"] digit+
\end{lstlisting}
\end{frame}




\section{Graphs}

\begin{frame}
  \frametitle{Definition}

\ExecuteMetaData[\catchfilefn]{def:Graph} 

\begin{example}
This simple graph has five vertices $\mathcal{V}=\set{v_0,\ldots,v_4}$ 
and eight edges.
\begin{center}
  \vcenteredhbox{\includegraphics{../../languages/asy/graphs/graphs00.pdf}}
  \qquad
  $\mathcal{E}=\set{\set{v_0,v_1},\set{v_0,v_2},\,\ldots\,\set{v_3,v_4}}$
\end{center}
\end{example}

\ExecuteMetaData[\catchfilefn]{discussion:EdgeNotation} 

\ExecuteMetaData[\catchfilefn]{discussion:GraphVariants} 
\pause
\ExecuteMetaData[\catchfilefn]{def:Subgraph} 
\end{frame}


\begin{frame}
  \frametitle{Recall: graph topics}

\ExecuteMetaData[\catchfilefn]{def:Adjacent} 

\pause

\ExecuteMetaData[\catchfilefn]{def:AdjacencyMatrix} 

\ExecuteMetaData[\catchfilefn]{lem:MatrixPowersAdjacencyMatrix} 
\end{frame}


\begin{frame}
\ExecuteMetaData[\catchfilefn]{def:GraphColoring} 

\begin{example}
We want to schedule final exams for the CS courses
1007, 3137, 3157, 3203, 3261, 4115, 4118, 4156.
These classes have students in common.
1007-3137,
1007-3157, 3137-3157,
1007-3203,
1007-3261, 3137-3261, 3203-3261,
1007-4115, 3137-4115, 3203-4115, 3261-4115,
1007-4118, 3137-4118,
1007-4156, 3137-4156, 3157-4156.
What is the minimum number of slots?
\end{example}

\pause
\ExecuteMetaData[\catchfilefn]{def:GraphIsomorphism} 

\ExecuteMetaData[\catchfilefn]{discussion:GraphIsomorphism}

If graphs are isomorphic then
associated vertices have the same degree.
Thus graphs with different degree sequences are not isomorphic.
And, if the the degree sequences are equal then they
help us construct an isomorphisms, if there is one.
(There are graphs with the same degree sequence that are not isomorphic.)
\end{frame}





% ==========================================
\section{Problems}
\begin{frame}{Problems, Algorithms, Programs}

\begin{itemize}
\item
A problem is a job, a task that we want done.
Often a problem encompasses a family of tasks.

\pause\item
An algorithm describes at a high level a way to solve the problem
that is effective, that is computable on a mechanism.
An algorithm
should be described in a way that is detailed enough that implementing it is
routine for an experienced professional.

\pause\item
A program is an implementation of an algorithm,
typically expressed in a formal computer language.
It is designed to be executed on a particular computing platform.
\end{itemize}

\begin{example}
One problem is, given a natural number, to find its prime factorization.
One algorithm to solve that problem is to use brute force, that is, to
check every number less than the input.
We could implement that with a program written in Scheme.
\end{example}
\end{frame}


\begin{frame}{Points about algorithms}
While an algorithm is an abstraction, it is nonetheless tied to
an underlying computing model.
We have seen the model of Turing machines.
The most common model is the RAM machine, consisting of a CPU with access
to memory, where each cell tholds a single atom.
Another model is distributted computation such as SETI at home.

\pause
Strictly speaking, to specify the algorithm,
a complete description of a problem should include the form of the
inputs and outputs.
For instance, if we state a problem as, `to input a sequence of natural numbers
and return their product'
then we have not fully specified the input.
We might take the input to be 
a list of strings representing decimal numbers
or
we might take it as a single bit string
where each number $n$ is represented with $n$-many \str{1}'s
and numbers are separated by \str{0}'s.
This matters because the form of the input and output can change the algorithm
or our analysis of its behavior.

Nevertheless, for our purposes specifying the input in great
detail is going into the weeds.
In particular at this time
we only want to know in principle whether we can solve the problems with
mechanical computation and
by Church's Thesis that is not 
affected by the exact form of the input and output representations.
In the fifth chapter we will worry more about the timing.
\end{frame}


\begin{frame}{Problem types}
\begin{description}
\item[Function problem]
Produce an algorithm that has a single output for each input.

\pause\item[Optimization problem]
Produce an algorithm that find solutions that are best in some way.

\pause\item[Search problem]
Produce an algorithm that finds any value matching some criteria.

\pause\item[Decision problem]
Produce an algorithm that answers `yes' or `no'.

A common case is a \alert{language recognition problem}, 
where we are given a language and asked for an algorithm that 
will decide membership in that language.
\end{description}

\pause
Search problems and decision problems are related.
For one, if you can solve a search problem then you can solve the
associated decision problem quickly.
For instance, consider 
the search problem that takes in a string~$\sigma$ and a substring~$\tau$,
returns the starting index of $\tau$ in~$\sigma$ if it is there, 
and returns $0$ if not.
The associated decision problem just asks if $\tau$ is a substring of~$\sigma$,
yes or no.

In the other direction,
if you could quickly solve the decision problem
then to solve the search problem you could look at the initial segments
of~$\sigma$: the initial substring of length~$0$, the initial substring
of length~$1$, etc., and apply the decision problem to each in turn.
That is, if we can solve the decision problem quickly then
we can use it to solve the search problem, but with some overhead.
\end{frame}
\end{document}




