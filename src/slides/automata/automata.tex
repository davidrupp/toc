\RequirePackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234 and https://www.latex4technics.com/?note=2uab
\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

\usepackage{../presentation,../presentationfonts}
\usepackage{../../grammar}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Automata}

\author{Jim Hef{}feron}
\institute{
  Mathematics and Statistics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Automata}

\usepackage{catchfilebetweentags} % read text from background.tex
\newcommand{\catchfilefn}{../../automata/automata.tex}
% Keep from swallowing end-of-lines
%   https://tex.stackexchange.com/a/40704/121234
\usepackage{etoolbox}
\makeatletter
\patchcmd{\CatchFBT@Fin@l}{\endlinechar\m@ne}{}
  {}{\typeout{Unsuccessful patch!}}
\makeatother

\usepackage{xr}
  \externaldocument{../../book.aux}
\usepackage{cleveref}

% \usepackage{multirow,bigstrut}
\begin{document}
\begin{frame}
  \titlepage
\end{frame}




\section{Finite State machines}
% ------------------------------------------------------
\begin{frame}
  \frametitle{Definitions}
\ExecuteMetaData[\catchfilefn]{def:FSM}

\begin{center}
  \includegraphics{../../asy/share/machine02.pdf}
\end{center}
\end{frame}


\begin{frame}
\begin{example}
This machine has input alphabet 
$\Sigma=\B=\set{\str{0},\str{1}}$.
The start state is $q_0$ (we take the convention that this is always the
start state).
It has one accepting state $F=\set{q_2}$
The set of states is $Q=\set{q_0,q_1,q_2}$
It will accept strings that contain at least two \str{1}'s.
\begin{center}
  \includegraphics{asy/fsm00.pdf}
\end{center}
The transition function is just a tabular version of the diagram.
The $\acceptingstatemark$ next to $q_2$ marks
it as an accepting state.
\begin{center}
\begin{transitionfunction}
      &\str{0}    &\str{1}   \\
    \cline{2-3}
    $q_0$ &$q_0$ &$q_1$   \\
    $q_1$ &$q_1$ &$q_2$    \\
    \acceptingstate
    $q_2$ &$q_2$ &$q_2$  \\
\end{transitionfunction}
\end{center}

\end{example}
\end{frame}


\begin{frame}
\begin{example}
\ExecuteMetaData[\catchfilefn]{ex:FSAValidIntegers}
\begin{center}  \small
  \vcenteredhbox{\includegraphics{../../automata/asy/fsa/fsa07.pdf}}
  \hspace{3em}
  \begin{transitionfunction}
      &\str{+}, \str{-} &\str{0}, \ldots{} \str{9} &\tabulartext{else}   \\
    \cline{2-4}
    $q_0$ &$q_1$ &$q_2$ &$e$   \\
    $q_1$ &$e$   &$q_2$ &$e$   \\
    \acceptingstate $q_2$ &$e$   &$q_2$ &$e$   \\
    $e$   &$e$   &$e$   &$e$   \\
  \end{transitionfunction}
  \end{center}
\end{example}
\pause
\begin{example}
This machine accepts only the single string \str{HEF}.
\begin{center}
  \includegraphics{asy/fsm01.pdf}
\end{center}
\end{example}
\end{frame}


\begin{frame}
\begin{example}
This machine accepts only strings where \str{HEF} is the prefix.
\begin{center}
  \includegraphics{asy/fsm02.pdf}
\end{center}
\end{example}
\pause
\begin{example}
This machine accepts only strings where \str{HEF} is the suffix.
\begin{center}
  \includegraphics{asy/fsm03.pdf}
\end{center}
\end{example}
\end{frame}




\begin{frame}
\begin{example}
This machine accepts only strings where \str{HEF} is a substring.
\begin{center}
  \includegraphics{asy/fsm04.pdf}
\end{center}
\end{example}
\pause
\begin{example}
This machine accepts only strings consisting of
zero or more repetitions of \str{HEF}.
\begin{center}
  \includegraphics{asy/fsm05.pdf}
\end{center}
\end{example}
\end{frame}




\begin{frame}
\begin{example}
This machine accepts only strings starting with zero or more 
copies of \str{HEF}.
\begin{center}
  \includegraphics{asy/fsm06.pdf}
\end{center}
\end{example}
\pause
\begin{example}
This machine accepts only the strings \str{CAT} or \str{DOG}.
\begin{center}
  \includegraphics{asy/fsm07.pdf}
\end{center}
\end{example}
\end{frame}


\begin{frame}
\begin{example}
This machine accepts only strings with an even number of \str{a}'s.
\begin{center}
  \includegraphics{asy/fsm08.pdf}
\end{center}
\end{example}
\pause
\begin{example}
This machine accepts strings with at least three \str{a}'s.
\begin{center}
  \includegraphics{asy/fsm09.pdf}
\end{center}
\end{example}
\end{frame}



\begin{frame}{Configurations}
\ExecuteMetaData[\catchfilefn]{discussion:FSMConfiguration}
\end{frame}

\begin{frame}{Configurations}
A \definend{configuration} of a Finite State machine $\FSM$ is 
a pair $\mathcal{C}=\sequence{q,\tau}$, where
$q$ is a state and $\tau$ is a string of elements 
from the tape alphabet~$\tau\in\kleenestar{\Sigma}$.
The \definend{initial configuration}
is~$\mathcal{C}_0=\sequence{q_0,\tau}$,
where $\tau$ is the \definend{input}.

\begin{example}
Give this machine the input $\sigma=\str{abababb}$.
\begin{center}
  \includegraphics{asy/fsm09.pdf}
\end{center}
The transition function~$\Delta$ determines how
the machine moves step-by-step, from configuration to configuration,
in response to the input.
\begin{align*}
    \mathcal{C}_0=\sequence{q_0,\str{abababb}}
    &\yields \mathcal{C}_1=\sequence{q_1,\str{bababb}}  \\
    &\yields \mathcal{C}_2= \sequence{q_1,\str{ababb}}  \\
    &\yields \mathcal{C}_3= \sequence{q_2,\str{babb}}  \\
    &\yields \mathcal{C}_4 = \sequence{q_2,\str{abb}}   \\
    &\yields \mathcal{C}_5 = \sequence{q_3,\str{bb}}  \\
    &\yields \mathcal{C}_6 = \sequence{q_3,\str{b}}  \\
    &\yields \mathcal{C}_7=  \sequence{q_3,\emptystring}
\end{align*}
The machine accepts $\sigma$ because when $\tau=\emptystring$ 
then the state,~$q_3$, is accepting.
\end{example}

\end{frame}



\begin{frame}{Language of a machine}
\ExecuteMetaData[\catchfilefn]{def:FSMLanguage}

\begin{example}
This machine accepts strings representing a natural number
that is a multiple of three, such as \str{15} and~\str{5013}.
\begin{center}
  \vcenteredhbox{\includegraphics{../../automata/asy/fsa/fsa12.pdf}}
\end{center}
The language is this, where $\Sigma=\set{\str{0}, \ldots\,\str{9}}$.
\begin{equation*}
  \lang(\FSM)=\set{\sigma\in\kleenestar{\Sigma}
    \suchthat\text{$\sigma$ is the decimal representation of a multiple of three}}
\end{equation*}
\end{example}
\end{frame}

\begin{frame}[fragile]{Finite State machines translate easily to code}
Here is a Python implementation of the multiple of three machine.
\begin{lstlisting}
def multiple_of_three_fsm(s):
    state = 0
    for ch in s:
        if state == 0:
            if ch in set([0,3,6,9]):
                state = 0
            elif ch in set([1,4,7]):
                state = 1
            else:
                state = 2
        elif state == 1:
            if ch in set([0,3,6,9]):
                state = 1
            elif ch in set([1,4,7]):
                state = 2
            else:
                state = 0
        else:
            if ch in set([0,3,6,9]):
                state = 2
            elif ch in set([1,4,7]):
                state = 0
            else:
                state = 1
    if state == 0:
        return True
    else:
        return False      
\end{lstlisting}
\end{frame}

\begin{frame}{Extended transition function}
\ExecuteMetaData[\catchfilefn]{def:ExtendedTransitionFcn}

\begin{example}
Find the extended transition function for this machine
\begin{center}
  \includegraphics{asy/fsm09.pdf}
\end{center}
for all strings of length less than four.
\end{example}
\end{frame}







% ===========================================
\section{Nondeterminism}
\begin{frame}{Recall: determinism} 
The definition of a Turing machine is that
it is a set of four-tuple instructions  $q_pT_pT_nq_n$
whose effect is to govern the transitinos that me machine makes,
in moving from configuration to configuration.
That set of instructions is subject to the restriction that it must be
deterministic\Dash no four-tuples can begin with 
the same $q_pT_p$.

Thus, over the set of instructions $q_pT_pT_nq_n\in\TM$,
the association of present pair $q_pT_p$ with next pair~$T_nq_n$,
defines a function, the \definend{transition function}
or \definend{next-state function}
$\map{\Delta}{Q\times \Sigma}{\Sigma\cup\set{\str{L},\str{R}}\times Q}$.  
Briefly, these machines satisfy that 
a present configuration determines a unique next state. 
The definition of Finite State machine has the same restriction.

\pause
We will now see what happens when we drop that requirement.
We do it here for Finite State machines and we will do it in the next chapter
for Turing machines.

We will see two ways to informally think of nondeterminism.
We will see examples of nondeterminism, and its uses.
In particular, we
will see that nondetermistic Finite State machines
machines do the same jobs as deterministic
ones. 

We start with an example and trace through the computation.
\end{frame}


\begin{frame}
Here, leaving $q_0$ are two arrows labelled~\str{0}.
Also, out of $q_1$ comes no~\str{1} arrow.
\begin{center}\small
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm00.pdf}}
\end{center}
Give it input \str{00001}.
\begin{center}\small
  \only<2>{\includegraphics{asy/nfsm02.pdf}}%
  \only<3>{\includegraphics{asy/nfsm03.pdf}}%
  \only<4>{\includegraphics{asy/nfsm04.pdf}}%
  \only<5>{\includegraphics{asy/nfsm05.pdf}}%
  \only<6>{\includegraphics{asy/nfsm06.pdf}}%
  \only<7>{\includegraphics{asy/nfsm07.pdf}}%
  \only<8>{\includegraphics{asy/nfsm08.pdf}}%
\end{center}

\end{frame}




\begin{frame}{First mental model: many children}
In that example, when we had a choice we forked children to take each
possibility.
This is a natural approach.
\begin{itemize}
\item
Consider a grammar \mbox{S\produces{}\trm{a}S\syntaxor\trm{b}A}.
You have a string~$\sigma$ and want to know if it has a derivation.
The challenge is that, for instance,
from S you can do two different things;
which one will work? 
If you wrote a program for this then you might instead try every
possible derivation, perhaps with a breadth-first traversal of the tree of all
derivations.

\pause\item
The Travelling Salesman Problem also illustrates.
Look at trips that visit every state capitol in the 
forty eight contiguous US~states.
For instance, we may want to know if there is a cycle of less than 
$16\,000$ kilometers.
Start at Montpelier. 
We could fork forty seven child processes, one for each potential next capital.
The process assigned Albany, for instance, would know that the trip so far is
$126$~kilometers. 
Each child would then fork its own children, forty six of them.
At the end, if there is
a trip of less than $16\,000$ kilometers then some process knows it.
If even one process is a success then we consider
the overall search a success.
This is nondeterministic in that during it
the machine has many process and
in that sense the machine is simultaneously in many different states.
\end{itemize}
\end{frame}


\begin{frame}{Second mental model: guessing}
The above machine accepts a string if it ends in two~\str{0}'s and a~\str{1}.
When we feed it the input \str{00001} the problem the machine faces is
when it should stop going around $q_0$'s loop and start to the right.
The example shows the machine accepting this input
so the machine has solved this problem\Dash viewed from the outside,
we could say that the
machine has guessed correctly.

\pause
This is jarring since  
we typically don't expect computers to guess.
We can instead imagine that the machine is
furnished with the answer (``go around twice, then off to the right'') and
only has to check that answer.
The furnisher is often personified as a demon.
\begin{center}
  \includegraphics[height=0.8in]{../../automata/pix/flauros.jpg}
\end{center}
\end{frame}

\begin{frame}{What the two views share}
In either mental model, this is an existence test\Dash 
the machine accepts the input if there exists a sequence of configurations
that ends in an accepting state.
\end{frame}


\begin{frame}
\frametitle{Definition}

\ExecuteMetaData[\catchfilefn]{def:NondeterministicFiniteStateMachine}
\ExecuteMetaData[\catchfilefn]{def:NFSMAccepted}
\pause
\begin{example}
This nondeterministic
garage door opener listener waits to hear the remote control send
the signal \str{0101110}.
\begin{center}
  \includegraphics{../../automata/asy/nfsm/nfsm11.pdf}
\end{center}
That is, it accepts the language
$\set{\sigma\concat\str{0101110}\suchthat \sigma\in\kleenestar{\B}}$
of strings with that suffix.
\end{example}

The listener will end in an accepting 
state if it 
hears a string $\sigma=\str{010101110}$, with three \str{01}'s.
How does this mechanism guess that it should ignore
the first \str{01} but pay attention to the second?

As earlier, it is strange to say that a machine ``guesses.''
We will show how to convert a nondeterministic Finite State
machine into  deterministic one.
So we can think of this
as an abbreviation for a deterministic machine,
which obviates the paradox of guessing, at least for Finite State machines.
\end{frame}

\begin{frame}
\begin{example}
This nondeterministic Finite State machine 
accepts only those strings ending in \str{HEF}.
\begin{center}
  \includegraphics{asy/nfsm11.pdf}
\end{center}
The alphabet~$\Sigma$ is the capital ASCII letters.

Contrast this machine with what we got when we did this job
using an explicit deterministic machine.
\begin{center}
  \includegraphics{asy/fsm03.pdf}
\end{center}
\end{example}
\end{frame}


\begin{frame}
\begin{example}
This nondeterminstic Finite State machine~$\FSM$  
with alphabet $\Sigma=\set{\str{a},\str{b}}$
accepts an input string only
if it has \str{a} in the third position from the end.
\begin{center}
  \includegraphics{asy/nfsm09.pdf}
\end{center}
Thus 
$\lang_{\FSM}=\set{\str{aaa}, \str{aab}, \str{aba}, \str{abb},\str{aaaa}, \str{aaab},\str{aaba},\str{aabb},\str{baaa},\str{baab},\ldots}$.
\end{example}

\pause
\begin{example}
This machine~$\hat{\FSM}$ with alphabet $\Sigma=\set{\str{a},\str{b}}$ 
accepts an input string only
if it contains a substring
with a \str{1} followed by any number of \str{0}'s
including zero-many \str{0}'s, 
followed by a second \str{1}.
\begin{center}
  \includegraphics{asy/nfsm16.pdf}
\end{center}
Thus 
$\lang_{\hat{\FSM}}=\set{\str{11}, \str{011}, \str{110}, \str{111},\str{0011}, \str{0101},\str{0110},\str{0111},\ldots}$.
\end{example}
\end{frame}



\begin{frame}{$\varepsilon$ transitions}
\ExecuteMetaData[\catchfilefn]{discussion:epsilonTransitions}

\pause
\begin{example}
On the left the machine accepts strings with 
prefix \str{ab}.
The one on the right accepts strings with suffix \str{ab}.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/nfsm12.pdf}}
  \hspace{4em}
  \vcenteredhbox{\includegraphics{asy/nfsm13.pdf}}
\end{center}
This puts them together serially.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/nfsm14.pdf}}
\end{center}
\end{example}
So it accepts only strings that begin with \str{ab} and end with \str{ab}.
\end{frame}

\begin{frame}
\begin{example}
We can take the same two machines, 
one that accepts strings with 
prefix \str{ab} and
one that accepts strings with suffix \str{ab}.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/nfsm12.pdf}}
  \hspace{4em}
  \vcenteredhbox{\includegraphics{asy/nfsm13.pdf}}
\end{center}
and put them together in parallel.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/nfsm15.pdf}}
\end{center}
The bottom machine accepts strings that begin with \str{ab} 
or end with \str{ab}.
\end{example}

\pause
\begin{example} 
A $\varepsilon$ transition edge can also take a machine and
give it the ``any number of repetitions'' effect.
That is, without that edge this machine's language is
$\set{\str{ab}}$
while adding it makes the lanaguage  
$\set{(\str{ab})^n\suchthat n\in\N}$.
\begin{center}
  \includegraphics{../../automata/asy/nfsm/nfsm15.pdf}
\end{center}
\end{example}
\end{frame}


\begin{frame}{Equivalence of the machine types}
% \begin{lemma}
% For every nondeterministic Finite State machine there is a deterministic
% Finite State machine that accepts the same language.
% \end{lemma}

\ExecuteMetaData[\catchfilefn]{th:dfsmEquivndfsm}

\end{frame}





% =======================================================
\section{Regular expressions}

\begin{frame}{Examples}
There is a pattern to the languages accepted by a Finite State machine.
We can describe such languages with \alert{regular expressions}.
Examples first, then a definition.

\pause
\ExecuteMetaData[\catchfilefn]{ex:RegexOne}

\pause
\ExecuteMetaData[\catchfilefn]{ex:RegexTwo}

\pause
\begin{example}
The strings that are accepted by this machine match \re{a*ba*}.
\begin{center}
  \includegraphics{../../automata/asy/fsa/fsa14.pdf}
\end{center}
\end{example}
\end{frame}


\begin{frame}{Definition: syntax}
\begin{definition}
\ExecuteMetaData[\catchfilefn]{def:RESyntax}
\end{definition}
\end{frame}

\begin{frame}{Semantics}
\ExecuteMetaData[\catchfilefn]{discussion:RESemantics}
\end{frame}


\begin{frame}{Examples}
\ExecuteMetaData[\catchfilefn]{ex:RegexThree}
\pause
\ExecuteMetaData[\catchfilefn]{ex:RegexFour}
\pause
\begin{example}
\ExecuteMetaData[\catchfilefn]{ex:RegexFive}
\end{example}
\pause
\ExecuteMetaData[\catchfilefn]{ex:RegexSix}
\end{frame}


\begin{frame}{More examples}

\begin{example}
Produce a regular expression to match each description.
For each, take the alphabet to be~$\B\set{\str{0},\str{1}}$.
(1)~The set of strings that end in three consecutive \str{1}'s. 
\pause
(2)~The set of strings with at least one~\str{0}.
\pause
(3)~The strings containing at least three~\str{1}'s.
\pause
(4)~The set of strings starting with a digit other than the one
it ends with.
\end{example}

\begin{example}
Produce a regular expression for each.
Take the alphabet to be the lower-case ASCII letters.
Use \url{http://joshua.smcvt.edu/cgi-bin/test_regex.py}.
(1)~Words containing both an \str{e} and a \str{u}.
\pause
(2)~Words having \str{f} as the second letter.
\pause
(3)~How many words start in \str{th}?
\pause
(4)~Words starting with \str{w} and containing a \str{y}.
\end{example}
\end{frame}
% import re
% import sys

% file = open(sys.argv[2], "r")

% for line in file:
%      if re.search(sys.argv[1], line):
%          print line,

\begin{frame}{Kleene's Theorem}
\ExecuteMetaData[\catchfilefn]{th:KleenesTheorem}

\pause
\ExecuteMetaData[\catchfilefn]{lem:KleenesThmRegexToMachine}

\proofname\hspace{0.6em} 
We will show that for any regular expression~$R$ there is a machine that
accepts strings matching that expression.

\ExecuteMetaData[\catchfilefn]{pf:KleenesThmRegexToMachinei}
  \begin{center}
    \includegraphics{../../automata/asy/nfsm/nfsm32.pdf}
    \hspace{2em}
    \includegraphics{../../automata/asy/nfsm/nfsm33.pdf}
    \hspace{2em}
    \includegraphics{../../automata/asy/nfsm/nfsm34.pdf}
  \end{center}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{pf:KleenesThmRegexToMachineii}\qedsymbol
\end{frame}



\begin{frame}
\ExecuteMetaData[\catchfilefn]{lem:KleenesThmMachineToRegex}

The idea is to start with a Finite State machine and eliminate its states
one at a time, keeping the accepted language the same, as here.
\begin{center}
\includegraphics{../../automata/asy/nfsm/nfsm35.pdf}
\hspace{2em}
\includegraphics{../../automata/asy/nfsm/nfsm36.pdf}
\end{center}
For this we generalize transition graphs to allow
edge labels that are regular expressions.

\pause
A bit more detail:~take \re{A}, \re{B}, \re{C}, and~\re{D} to be
regular expressions.
\begin{center}
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm64.pdf}}
  \hspace{2em}
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm65.pdf}}
\end{center}
Start by introducing a new start state guaranteed to have
no incoming edges, $e$,
and a new final state guaranteed to be unique,~$f$.
Then eliminate $q_1$ as below.
\begin{center}
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm66.pdf}}
\end{center}
\end{frame}



\begin{frame}
The proof is in the book.
Here we will give an example. 
\begin{example}
Consider~$\FSM$ on the left.
Introduce $e$ and~$f$ to get $\hat{\FSM}$ on the right.
\begin{center}
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm48.pdf}}
  \hspace{2em}
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm49.pdf}}
\end{center}
\pause
Start by eliminating $q_2$.
That gives this machine.
\begin{center}
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm50.pdf}}
\end{center}
\end{example}
\end{frame}
\begin{frame}
Next eliminate $q_1$.
\begin{center}
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm51.pdf}}    
\end{center}
\pause
Finally, eliminate $q_0$.
\begin{center}
  \vcenteredhbox{\includegraphics{../../automata/asy/nfsm/nfsm52.pdf}}    
\end{center}
That regular expression simplifies.
For instance, \re{a$\varepsilon$}=\re{a}.
The result is \re{(a(b|ab*b))*($\varepsilon$|a)}.
\end{frame}




% ==============================================
\section{Regular languages}

\begin{frame}{Definition}
\ExecuteMetaData[\catchfilefn]{def:RegularLangs}

\begin{example}
(1)~The set
$\set{\str{a}\str{b}^n\suchthat n\in\N}
  =\set{\str{a}, \str{ab}, \str{abb}\,\ldots}$ is a regular language,
(2~any finite set of strings is a regular language. 
\end{example}

\pause
In the proof of Kleene's Theorem we saw that if two languages
are regular then their union is also regular.
We have also seen
that if two languages are regular then their concatenation
is regular.
We say that a structure is \definend{closed} under an operation if
performing that operation on its members always yields another member.

\ExecuteMetaData[\catchfilefn]{lem:RegLangsClosedUnderUnionConcatStar}
\begin{proof}
  In the first half of Kleene's Theorem
  we did the regular expression operations of pipe,
  concatenation, and Kleene star.
  Those correspond to these set operations:
  for instance, if $R_0$ is a regular expression describing the language
  $\lang_0$, and $R_1$ describes $\lang_1$, then the regular expression
  $\re{$R_0$|$R_1$}$ describes $\lang_0\cup\lang_1$.
\end{proof}
\end{frame}



\begin{frame}
\ExecuteMetaData[\catchfilefn]{th:RegLangsClosedCompIntDiff}

\ExecuteMetaData[\catchfilefn]{pf:RegLangsClosedCompIntDiff}
\end{frame}


\begin{frame}
\begin{example}
Let $\Sigma=\B=\set{\str{0},\str{1}}$.
We could show that the language
$\lang=\set{\sigma\str{1}\suchthat \sigma\in\kleenestar{\Sigma}}$
is regular by producing a Finite State machine to accept it.
But closure is easier.
That language is the concatenation $\lang=\lang_0\concat\lang_1$
of
$  \lang_0=\kleenestar{\Sigma}$
and
$  \lang_1=\set{\str{1}}$,
which are both clearly regular.
Regular languages are closed under concatenation, so $\lang$ is regular.
\end{example}

\pause
\begin{example}
The language 
\begin{equation*}
\lang=\set{\sigma\in\kleenestar{\set{\str{a},\str{b},\str{c}}}
  \suchthat \text{$\sigma$ does not have the substring \str{acb}}}
\end{equation*}
it the complement of this one.
$\setcomp{\lang}=\set{\sigma\in\kleenestar{\set{\str{a},\str{b},\str{c}}}
  \suchthat \text{$\sigma$ has the substring \str{acb}}}$.
The language $\setcomp{\lang}$ is regular because it is 
described by a regular expression,
\re{(a|b|c)*acb(a|b|c)*}.
\end{example}

\pause
\begin{example}
We can show that the language 
\begin{equation*}
\lang=\set{\sigma\in\kleenestar{\set{\str{a},\str{b}}}
  \suchthat \text{$\sigma$ contains the substrings \str{acb} and \str{cbc}}}
\end{equation*}
is regular by constructing a Finite State machine
for each substring, or give a regular expression, and then cite that the regular
languages are closed under intersection.
\end{example}
\end{frame}


% ========================================================
\section{Non-regular languages}

\begin{frame}{Observation: periodicity}
\ExecuteMetaData[\catchfilefn]{ex:FSMLoopingi}
\begin{center}
  \includegraphics{../../automata/asy/nfsm/nfsm58.pdf}
\end{center}
\ExecuteMetaData[\catchfilefn]{ex:FSMLoopingii}
\end{frame}

\begin{frame}{Pumping lemma}
\ExecuteMetaData[\catchfilefn]{th:PumpingLemma}

\proofname.\hspace{0.6em}
\ExecuteMetaData[\catchfilefn]{pf:PumpingLemmai}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{pf:PumpingLemmaii}\qedsymbol

\pause
\begin{example}
Show that each language over $\Sigma=\set{\str{a},\str{b}}$ is not a regular:
(1)~$\set{\str{a}^n\str{b}^n\suchthat n\in\N}$,
(2)~$\set{\str{a}^n\str{b}^m\suchthat n=m+1}$,
(3)~the set of palindromes.
\end{example}
\end{frame}




% ========================================================
\section{Pushdown machines}
\begin{frame}{Introduction}
\ExecuteMetaData[\catchfilefn]{discussion:StackIntroi}

\ExecuteMetaData[\catchfilefn]{discussion:StackIntroii}
\begin{center}
  \vcenteredhbox{\includegraphics[height=1.1in]{../../automata/pix/dishes.jpg}}
  \hspace{4em}
  \vcenteredhbox{\includegraphics{../../automata/asy/machine/machine02.pdf}}
  \hspace{1.25em}
  \vcenteredhbox{\includegraphics{../../automata/asy/machine/machine03.pdf}}
  \hspace{1.25em}
  \vcenteredhbox{\includegraphics{../../automata/asy/machine/machine04.pdf}}
  \hspace{1.25em}
  \vcenteredhbox{\includegraphics{../../automata/asy/machine/machine05.pdf}}
\end{center}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{def:PushdownMachine}
\begin{center}
  \includegraphics{../../asy/share/machine03.pdf}
\end{center}
The book has the full details.
We will give a couple of examples.
\end{frame}

\begin{frame}
\begin{example}
\ExecuteMetaData[\catchfilefn]{ex:BalancedParensAcceptedPushdown}
\end{example}
\end{frame}


\begin{frame}
\begin{tapesequence}
  \begin{tapesequencetable}
    0 &\tapegraphic{../../automata/asy/machine/machine06.pdf} \\
    1 &\tapegraphic{../../automata/asy/machine/machine07.pdf} \\
    2 &\tapegraphic{../../automata/asy/machine/machine08.pdf} \\
    3 &\tapegraphic{../../automata/asy/machine/machine09.pdf} \\
    4 &\tapegraphic{../../automata/asy/machine/machine10.pdf} \\
    5 &\tapegraphic{../../automata/asy/machine/machine11.pdf} \\
    6 &\tapegraphic{../../automata/asy/machine/machine12.pdf} \\
    7 &\tapegraphic{../../automata/asy/machine/machine13.pdf} \\
  \end{tapesequencetable}
\end{tapesequence}
\end{frame}


\begin{frame}
\begin{example}
\ExecuteMetaData[\catchfilefn]{ex:PalindromesUsingMiddleMarkeri}
\end{example}
\end{frame}


\begin{frame}
This computation has the machine accept \str{bacab}.
\begin{tapesequence}
  \begin{tapesequencetable}
    0 &\tapegraphic{../../automata/asy/machine/machine25.pdf} \\
    1 &\tapegraphic{../../automata/asy/machine/machine26.pdf} \\
    2 &\tapegraphic{../../automata/asy/machine/machine27.pdf} \\
    3 &\tapegraphic{../../automata/asy/machine/machine28.pdf} \\
    4 &\tapegraphic{../../automata/asy/machine/machine29.pdf} \\
    5 &\tapegraphic{../../automata/asy/machine/machine30.pdf} \\
    6 &\tapegraphic{../../automata/asy/machine/machine31.pdf} \\
  \end{tapesequencetable}
\end{tapesequence}
\end{frame}



\begin{frame}{Nondeterministic pushdown machines}
\ExecuteMetaData[\catchfilefn]{discussion:NondeterminsticPushdown}
\end{frame}


\begin{frame}
\begin{example}
\ExecuteMetaData[\catchfilefn]{ex:NondeterministicPushDownAcceptsPalindromes}
\end{example}
\end{frame}

\begin{frame}
Here the machine
accepts the string \str{0110},
using instructions $0$, $9$, $16$, and~$17$.
\begin{tapesequence}
  \begin{tapesequencetable}
    0 &\tapegraphic{../../automata/asy/machine/machine14.pdf} \\
    1 &\tapegraphic{../../automata/asy/machine/machine15.pdf} \\
    2 &\tapegraphic{../../automata/asy/machine/machine16.pdf} \\
    3 &\tapegraphic{../../automata/asy/machine/machine17.pdf} \\
    4 &\tapegraphic{../../automata/asy/machine/machine18.pdf} \\
  \end{tapesequencetable}
\end{tapesequence}
\end{frame}
\begin{frame}
Here is the machine accepting \str{01010}.
\begin{tapesequence}
  \begin{tapesequencetable}
    0 &\tapegraphic{../../automata/asy/machine/machine19.pdf} \\
    1 &\tapegraphic{../../automata/asy/machine/machine20.pdf} \\
    2 &\tapegraphic{../../automata/asy/machine/machine21.pdf} \\
    3 &\tapegraphic{../../automata/asy/machine/machine22.pdf} \\
    4 &\tapegraphic{../../automata/asy/machine/machine23.pdf} \\
    5 &\tapegraphic{../../automata/asy/machine/machine24.pdf} \\
  \end{tapesequencetable}
  \end{tapesequence}
\end{frame}


\begin{frame}{Relation between machine types and languages}
\begin{center} \small
  % \mbox{Languages over $\B$:\ }
  \vcenteredhbox{\includegraphics{../../automata/asy/pda/pda00.pdf}}
  \hspace{2em}
  \begin{tabular}{rl}
    \tabulartext{Class}  &\tabulartext{Machine type} \\
    \hline
    $A$ &Finite State, including nondeterministic\rule{0em}{2.15ex} \\
    $B$ &Pushdown \\
    $C$ &nondeterministic Pushdown \\
    $D$ &Turing \\
  \end{tabular}
\end{center}
\end{frame}
\end{document}




