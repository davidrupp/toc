\section{Functions}\label{sec:Functions}\index{functions|(}

A function is an input/output relationship:~for each
input value there is an output value, and that output value is unique.

One example is the association of each input natural number with 
an output number that is twice as big.
Another is the association of each string of characters over \str{a}--\str{z}
with the length of that string.
A third is the association of each polynomial 
$a_nx^n+\cdots+a_1x+a_0$ with a Boolean value $T$ or~$F$,
depending on whether $1$ is a root of that polynomial.

For the definition fix two sets, 
a \definend{domain}~$D$\index{domain}\index{function!domain} 
and a \definend{codomain}~$C$.\index{codomain}\index{function!codomain}
A \definend{function},\index{function}\index{function!definition} 
or \definend{map},\index{map|see {function}}
$\map{f}{D}{C}$ is a set of pairs
$(x,y)\in D\times C$ subject to the restriction that
every element~$x\in D$ appears in one and only one pair.
Where $(x,y)$ is one of the pairs we write $f(x)=y$ 
or $x\mapsto y$. 
(Note the difference between the arrow in $\map{f}{D}{C}$ and the one
in $x\mapsto y$). 
We say that~$x$ is an \definend{input}\index{input, to a function}\index{function!index} 
or \definend{argument}\index{argument, to a function}\index{function!argument} 
to the function,
and that~$y$ is an 
\definend{output}\index{output, from a function}\index{function!output} 
or \definend{value}.\index{value, of a function}\index{function!value}

The most important point is what a function isn't:~it isn't a 
formula.
The function that gives the presidents of the US, 
$f(0)=\text{George Washington}$, etc., has no sensible formula.
Nor can you produce a formula that returns World Series winners,
including next year's.  
Many functions are described by formulas, such as $f(m)=mc^2$,
and many functions are described by computer programs.
But what makes something a function is that for each input 
from the domain there is one and only one output from the codomain,
not that you can calculate those output values.

A function may take more than one input, for instance 
$\operatorname{dist}(x,y)=\sqrt{x^2+y^2}$.
We may call a function $2$-ary, or $3$-ary, etc.
The number of inputs is the function's \definend{arity}.
If the function takes only one input but that input is a tuple,
as with $x=(3,5)$, then we often drop the extra parentheses, so that
instead of $f(x)=f((3,5))$ we write $f(3,5)$.



\subsection*{Pictures}
We often illustrate functions using the familiar $xy$~axes;
here are graphs of $f(x)=x^3$ and $f(x)=\floor{x}$.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions000.pdf}}
  \hspace{2em plus 0.5fil}
  \vcenteredhbox{\includegraphics{appendix/asy/functions001.pdf}}
\end{center}
We also may illustrate functions using a bean diagram, 
which does not have the domain and the codomain mixed, but instead
separates them.
Here it pictures the action of the exclusive~or operator. 
\begin{center}
  \includegraphics{appendix/asy/functions002.pdf}
  \hspace{2em plus 1fil}
  \includegraphics{appendix/asy/functions005.pdf}
\end{center}
On the right is a variant of the bean diagram, showing the
absolute value function mapping integers to integers.


\subsection*{Codomain and range}
Where  $S\subseteq D$ is a subset of the domain, its 
\definend{image}\index{image under a function}\index{function!image under} 
is the set $f(S)=\set{f(s)\suchthat s\in S}$.
Thus, under the floor function the image of the positive reals is
the nonnegative reals.
The image of the entire domain is the 
\definend{range}\index{range of a function}\index{function!range}
of the function, $\operatorname{ran}(f)=f(D)=\set{f(s)\suchthat s\in S}$.
Note that the range is not the same as the codomain;\index{codomain versus range}
instead, the codomain is a convenient superset.
For instance, for the real function $f(x)=x^2$ we usually write 
$\map{f}{\R}{\R}$, although the range is the nonnegative reals. 

When we are defining a function and write 
$\map{f}{D}{C}$ we need that
every $f(x)$ is indeed an element of~$C$; usually it is clear but 
on occasion we need to verify it explicilty.


\subsection*{Domain}\index{domain}
Sometimes it is the domain that is a concern.
Examples of real number functions where the domain causes trouble
are that $f(x)=1/x$ 
is undefined at $x=0$, and that the infinite series
$g(r)=1+r+r^2+\cdots$ diverges when $r$ is outside the interval
$\open{-1}{1}$.
Formally, when we define the function 
we must specify the domain to eliminate such problems,
say by defining the domain of~$f$ as $\R=\set{0}$.
However, we are often casual about this and in this subject
if there are some elements of the domain where the function
is undefined we will say that it is a 
\definend{partial function}\index{partial function}\index{function!partial} 
(otherwise
it is a \definend{total function}).\index{total function}\index{function!total}

We sometimes want to cut the domain of a function back to a subset.
If $\map{f}{D}{C}$ is a function and $S\subseteq D$ then the 
\definend{restriction}\index{restriction}\index{function!restriction} 
is $\restrictionmap{f}{S}$ is the function with
domain~$S$ and codomain~$D$ defined by $\restrictionmap{f}{S}(x)=f(x)$.   


\subsection*{Well-defined}\index{well-defined}\index{function!well-defined}
The definition of a function contains  
the restriction that  
each domain element maps to one and only one 
codomain element, $y=f(x)$.
We say that functions are \definend{well-defined}.

When we are considering a relationship 
between $x$'s and $y$'s and
asking if it is a function, this restriction is typically the point at 
issue.\footnote{%
  Sometimes people say that they are, ``checking that the function is 
  well-defined.''
  In a strict sense this is confused, or at least awkward, since   
  if it is a function then it is by definition well-defined.
  However, all tigers have stripes but we do
  sometimes say ``striped tiger;''; natural language is funny that way.
  }
For instance, consider the relation between
$x,y\in\R$ where the square of $y$ is~$x$.
This is not a functional relationship
because if~$x=9$ then both $y=3$ and~$y=-3$ are related to~$x$.
Similarly, if we ask which real number angle~$y$ has a cosine of~$x=1/2$ then
there are many such numbers.
Or, if we are setting up a company's email we may decide to use a person's
first initial and last name but then realize that
there can easily be more than one, say, \str{rjones}.
The problem is that the relation connecting the email address to person is not 
well-defined\Dash it is ill-defined.

If a function is suitable for graphing on $xy$~axes then 
visual proof of well-definedness is that for any $x$ in the domain, 
the vertical line
at~$x$ intercepts the graph in exactly one point.


\subsection*{One-to-one and onto}
The definition of function has an asymmetry;~it 
requires that each domain element be in one and only one pair,
but it does not require the same of the codomain elements.

A function~$f$ is 
\definend{one-to-one},\index{one-to-one function}\index{function!one0-to-one} 
or \definend{$1$-$1$} or
an \definend{injection},\index{injection}\index{function!injection} 
if each codomain element is in at most one pair in~$f$.
This function is one-to-one because every element in the bean on the
right has at most one arrow ending at it.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions004.pdf}}
\end{center}
The most common way to verify that a function is one-to-one is to 
assume that $f(x_0)=f(x_1)$ and then deduce that therefore $x_0=x_1$.
If a function is suitable for graphing on $xy$~axes then 
visual proof is that for any $y$ in the codomain, the horizontal line
at~$y$ intercepts the graph in at most one point.

As the above picture suggests, 
where both the domain and codomain are finite, 
if the function is one-to-one then the domain has at most as many elements
as its codomain.

Note also that the restriction of a one-to-one function is one-to-one.

A function~$f$ is 
\definend{onto},\index{onto function}\index{function!onto} 
or a 
\definend{surjection},\index{surjection}\index{function!surjection} 
if each codomain element is in at least one pair in~$f$.
Thus, a function is onto if its codomain equals its range.
This function is onto because every element in the bean on the
right has at least one arrow ending at it.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions003.pdf}}
\end{center}
The most common way to verify that a function is onto is to 
start with a codomain element~$y$ and then 
produce a domain element~$x$ that maps to it.
If a function is suitable for graphing on $xy$~axes then 
visual proof is that for any $y$ in the codomain, the horizontal line
at~$y$ intercepts the graph in at least one point.

As the above picture suggests, where
the domain and codomain are finite, if a function is onto
then its domain has at least as many elements as its codomain.


\subsection*{Correspondence}
A function is a 
\definend{correspondence},\index{correspondence}\index{function!correspondence} 
or a \definend{bijection},\index{bijection} 
if it is both one-to-one and onto.
The left picture shows a correspondence between two finite sets, 
both with four elements,
and the right shows a correspondence between the natural numbers
and the primes.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions006.pdf}}
  \hspace{1em plus 1fil}
  \vcenteredhbox{\includegraphics{appendix/asy/functions007.pdf}}
\end{center}
The most common way to verify that a function is a correspondence is to 
separately verify that it is one-to-one and that it is onto.
If the function is suitable for graphing on $xy$~axes then 
visual proof is that for any $y$ in the codomain, the horizontal line
at~$y$ intercepts the graph in exactly one point.

As the above pictures suggests, where
the domain and codomain are finite, if a function is a correspondence
then its domain has exactly as many elements as its codomain.


\subsection*{Composition and inverse}
If $\map{f}{D}{C}$ and $\map{g}{C}{B}$ then their 
\definend{composition $\map{\composed{g}{f}}{D}{B}$}\index{composition}\index{function!composition} 
is given by $\composed{g}{f}\,(d)= g(\,f(d)\,)$.
For instance, the real functions $f(x)=\sin(x)$ and $g(x)=x^2$
have the composition $\composed{g}{f}=\big(sin(x)\big)^2$.

Composition does not commute.
For instance, with the functions from the prior paragraph 
the composition $\composed{f}{g}=\sin(x^2)$ is different.
It can fail to commute even more dramatically:~if
$\map{f}{\R^2}{\R}$ is given by $f(x_0,x_1)=x_0$ and 
$\map{g}{\R}{\R}$ is given by $g(x)=3x$ then
$\composed{g}{f}(x_0,x_1)=3x_0$ is perfectly sensible but
composition in the other order is not defined.

The composition of one-to-one functions is one-to-one, and the 
composition of onto functions is onto.
Of course then, the composition of correspondences is a 
correspondence. 

An \definend{identity function}\index{identity!function}\index{function!identity} 
$\map{\identity}{D}{D}$ has the action
$\identity(d)=d$ for all $d\in D$.
It acts as the identity element in function composition,
so that if $\map{f}{D}{C}$ then $\composed{f}{\identity}=f$
and if $\map{g}{C}{D}$ then $\composed{\identity}{g}=g$.
And, if $\map{h}{D}{D}$ then 
$\composed{h}{\identity}=\composed{\identity}{h}=h$.

Given $\map{f}{D}{C}$,
if $\composed{g}{f}$ is the identity function then 
$g$~is a \definend{left inverse}\index{left inverse}\index{function!left inverse}\index{inverse of a function!left} 
function of~$f$, or what is the
same thing, $f$ is a 
\definend{right inverse}\index{right inverse}\index{function!right inverse}\index{inverse of a function!right}  
of~$g$.
If $g$ is both a left and right inverse of~$f$ then
it is an 
\definend{inverse}\index{inverse of a function}\index{inverse of a function!two-sided}\index{function!inverse}\index{two-sided inverse} 
(or \definend{two-sided inverse}) of~$f$, denoted $f^{-1}$. 
If a function has an inverse then that inverse is unique.
A function has a two-sided inverse if and only if it is a 
correspondence.


\begin{exercises}
\begin{exercise} 
  Let $\map{f}{\R}{\R}$ be $f(x)=3x+1$ and 
  $\map{g}{\R}{\R}$ be $g(x)=x^2+1$.
\begin{exparts*}
\item 
  Show that $f$ is one-to-one and onto.
\item
  Show that $g$ is not one-to-one and not onto.
\end{exparts*}
\begin{answer}
\begin{exparts}
\item 
  For one-to-one, suppose that $f(x_0)=f(x_1)$ for $x_0,x_1\in\R$.
  Then $3x_0+1=3x_1+1$.
  Subtract the~$1$'s and divide by~$3$ to conclude that $x_0=x_1$.
  Thus~$f$ is one-to-one.   

  For onto, fix a member of the codomain~$c\in\R$.
  Observe that $d=(c-1)/3$ is a member of the domain and that 
  $f(d)=3\cdot ((c-1)/3)+1=(c-1)+1=c$.
  Thus $f$ is onto.  
\item
  The function~$g$ is not one-to-one because $g(2)=g(-2)$.
  It is not onto because no element of the domain~$\R$ is mapped by~$g$
  to the element $0$ of the codomain.     
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise} Show each.
\begin{exparts}
\item
Let $\map{g}{\R^3}{\R^2}$
be the projection map $(x,y,z)\mapsto(x,y)$
and let $\map{f}{\R^2}{\R^3}$ be the injection map
$(x,y)\mapsto(x,y,0)$.
Then $g$ is a left inverse of~$f$ but not a right inverse.
\item
  The function $\map{f}{\Z}{\Z}$ given by $f(n)=n^2$ has no left inverse.
\item
Where $D=\set{0,1,2,3}$ and $C=\set{10,11}$,
the function $\map{f}{D}{C}$ given by
$0\mapsto 10$, $1\mapsto 11$, $2\mapsto 10$, $3\mapsto 11$
has more than one right inverse.  
\end{exparts}
\begin{answer}
\begin{exparts}
\item
It is a left inverse because $\composed{g}{f}$ has the action
$(x,y)\mapsto(x,y,0)\mapsto(x,y)$ for all $x$ and~$y$.
It is not a right inverse because there is an 
input on which the composition $\composed{f}{g}$ is not the identity, namely
the action is 
$(1,2,3)\mapsto(1,2)\mapsto(1,2,0)$.
\item 
Observe that $f(2)=f(-2)=4$.
Any left inverse would have to map $4$ to~$2$ and also to map
$4$ to~$-2$.
That would be not well-defined, so no function is the left inverse.
\item
One right inverse is $\map{g_0}{C}{D}$ given by $10\mapsto 0$, 
$11\mapsto 1$.
A second is  
$\map{g_1}{C}{D}$ given by $10\mapsto 2$, 
$11\mapsto 3$.
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise} 
\begin{exparts}
\item 
  Where $\map{f}{\Z}{\Z}$ is~$f(a)=a+3$ and 
  $\map{g}{\Z}{\Z}$ is $g(a)=a-3$,
  show that $g$ is inverse to $f$.
\item
  Where $\map{h}{\Z}{\Z}$ is the function that returns
  $n+1$ if $n$ is even and returns $n-1$ if $n$ is odd,
  find a function inverse to~$h$.
\item
  If $\map{s}{\R^+}{\R^+}$ is~$s(x)=x^2$,
  find its inverse.
\end{exparts}
\begin{answer}
\begin{exparts}
\item
  First, the domain of each equals the codomain of the other
  so their composition is defined in both ways.
  Next, $\composed{g}{f}\,(a)=g(f(a))=g(a+3)=(a+3)-3=a$
  is the identity map.
  Similarly, $\composed{f}{g}\,(a)=(a-3)+3$ is also the identity map.
  Thus they are inverse.  
\item
  The inverse of~$h$ is itself.
  The domain and codomain are equal, as required. 
  If~$n$ is odd then $\composed{h}{h}\,(n)=h(n-1)=(n-1)+1=n$ (the second
  equality holds because $n-1$ is even in this case).
  Similarly if~$n$ is even then $\composed{h}{h}\,(n)=(n+1)-1=n$.  
\item
  The inverse of~$s$ is the map~$\map{r}{\R^+}{\R^+}$ given my 
  $r(x)=\sqrt{x}$.
  The domain of each is the codomain of the other,
  and each of $\composed{s}{r}$ and~$\composed{r}{s}$ is the identity function.
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise}
Let $D=\set{0,1,2}$ and~$C=\set{10,11,12}$.
Also let $\map{f,g}{D}{C}$ be
$f(0)=10$, $f(1)=11$, $f(2)=12$, and
$g(0)=10$, $g(1)=10$, $g(2)=12$.
Then:
\begin{exparts*}
\item verify that $f$ is a correspondence
\item construct an inverse for~$f$\!
\item verify that~$g$ is not a correspondence
\item show that~$g$ has no inverse. 
\end{exparts*}
\begin{answer}
This is a bean diagram of the function~$f$
\begin{center}
  \includegraphics{appendix/asy/functions008.pdf}
\end{center}
and this is a diagram of~$g$.
\begin{center}
  \includegraphics{appendix/asy/functions009.pdf}
\end{center}
\begin{exparts}
\item By inspection $f$ is both one-to-one and onto.
\item Reverse the association made by $f$ so that $f^{-1}(10)=0$,
  $f^{-1}(11)=1$, and~$f^{-1}(12)=2$.
\item The map~$g$ is not one-to-one since $g(0)=g(1)$.
  (In addition, $g$ is not onto since no input is sent to $11\in C$.) 
\item If a $h$ were the inverse of~$g$
  then both $h(10)=0$ and~$h(10)=1$, 
  which would violate the definition of a function.   
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise} \label{InteractionOneToONeOntoWithComposition}
\begin{exparts*}
\item
  A composition of one-to-one functions is one-to-one.
\item
  A composition of onto functions is onto.
  With the prior item this gives that a
  composition of correspondences is a correspondence.
\item
  If $\composed{g}{f}$ is one-to-one then $f$ is one-to-one.
\item
  If $\composed{g}{f}$ is onto then $g$ is onto.
\item
  If $\composed{g}{f}$ is onto, is $f$ onto?
  If it is one-to-one, is $g$ one-to-one?
\end{exparts*}
\begin{answer}
\begin{exparts}
\item
  Let $\map{f}{D}{C}$ and~$\map{g}{C}{B}$ be one-to-one.
  Suppose that $\composed{g}{f}\,(x_0)=\composed{g}{f}\,(x_1)$ for
  some $x_0,x_1\in D$.
  Then $g(f(x_0))=g(f(x_1))$.
  The function~$g$ is one-to-one and so, since the two values $g(f(x_0))$ 
  and~$g(f(x_1))$ are equal, the arguments $f(x_0)$ and~$f(x_1)$ are
  also equal.
  The function~$f$ is one-to-one and so $x_0=x_1$.
  Because $\composed{g}{f}\,(x_0)=\composed{g}{f}\,(x_1)$ implies that
  $x_0=x_1$, the composition is one-to-one.  
\item
  Let $\map{f}{D}{C}$ and~$\map{g}{C}{B}$ be onto.
  If $B$ is the empty set then the composition is onto, vacuously.
  Otherwise, fix an element of the codomain of the composition,~$b\in B$.
  The function~$g$ is onto so there is a $c\in C$ such that $g(c)=b$.
  The function~$f$ is onto so there is a $d\in D$ such that $f(d)=c$.
  Then $\composed{g}{f}(d)=b$, and so the composition is onto.
\item
  Let $\map{f}{D}{C}$ and~$\map{g}{C}{B}$ be such that 
  the composition $\composed{g}{f}$ is one-to-one.
  Suppose that~$f$ is not one-to-one.
  Then there are $d_0,d_1\in D$ with $d_0\neq d_1$ such that $f(d_0)=f(d_1)$.
  But this implies that $g(f(d_0))=g(f(d_1))$, contradicting that 
  the composition is one-to-one.  
\item
  Let $\map{f}{D}{C}$ and~$\map{g}{C}{B}$ be such that 
  the composition $\composed{g}{f}$ is onto.
  Suppose for a contradiction that~$g$ is not onto.
  Then for some~$b\in B$ there is no~$c\in C$ with~$g(c)=b$.
  But this means that there is no~$d\in D$ such that 
  $b=g(f(d))$, contradicting that the composition is onto.
  So $g$ must be onto.  
\item
  The answer to both questions is ``no.''
  
  Let $D=\set{0,1}$, $C=\set{10,11,12}$, and $B=\set{20, 21}$.
  Let $f$ map $0\mapsunder{} 11$ and~$1\mapsunder{} 12$.
  Let $g$ map $10\mapsunder{} 20$, $11\mapsunder{}20$, 
  and~$12\mapsunder{} 21$. 
  Then $\composed{g}{f}$ is onto because $\composed{g}{f}\,(0)=20$ and
  $\composed{g}{f}\,(1)=21$.
  But $f$ is not onto because no element of its domain~$D$ is mapped to the
  element $10$ of its codomain~$C$. 
  \begin{center}
    \includegraphics{appendix/asy/functions010.pdf}
  \end{center}
  The same function is an example of a composition $\composed{g}{f}$ 
  that is one-to-one but where
  the function performed second,~$g$, is not one-to-one.
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise} \label{PropertiesOfInverses}
Prove.
\begin{exparts}
\item
  A function has an inverse if and only if that 
  function is a correspondence.
\item 
  If a function has an inverse then that inverse
  is unique.
\item
  The inverse of a correspondence is a correspondence.  
\item
  If $f$ and $g$ are each invertible then so is 
  $\composed{g}{f}$, and $(\composed{g}{f})^{-1}=\composed{f^{-1}}{g^{-1}}$.
\end{exparts}
\begin{answer}
\begin{exparts}
\item
  We first prove that if the function $\map{f}{D}{C}$ has an
  inverse $\map{f^{-1}}{C}{D}$ then it must be  a correspondence.
  To verify that $f$ is one-to-one,
  suppose that~$d_0,d_1\in D$ are 
  such that $f(d_0)=f(d_1)=c$ for some $c\in C$.
  Consider~$f^{-1}(c)$.
  Because $\composed{f^{-1}}{f}$ is the identity map on~$D$ we have 
  both that $f^{-1}(c)=f^{-1}(f(d_0))=d_0$ 
  and that $f^{-1}(c)=f^{-1}(f(d_1))=d_1$.
  Thus $d_0=d_1$ and so $f$ is one-to-one.
   
  Next we verify that~$f$ is onto.
  Suppose otherwise, 
  so that there is a~$c\in C$ that is not associated with any~$d\in D$.
  The map~$\composed{f}{f^{-1}}$
  cannot give $c=\composed{f}{f^{-1}}\,(c)=f(f^{-1}(c))$ 
  because $c\notin\range(f)$, so this is impossible.
  Therefore~$f$ is onto.  

  To finish, we prove that if a map $\map{f}{D}{C}$
  is a correspondence then it has an inverse.
  We will define an association $\map{g}{D}{C}$ and show that
  it is well-defined.
  For each $c\in C$, let $g(c)$ be the element $d\in D$ 
  such that $f(d)=c$;
  note that there is such an element because $f$ is onto, and that
  there is a unique such element because $f$ is one-to-one.
  So $g$ is well-defined.
  That $\composed{g}{f}$ and $\composed{f}{g}$ are identity
  maps is clear.
\item
  Suppose that $\map{f}{D}{C}$ has two inverses $\map{g_0,g_1}{C}{D}$.
  We will show that the two make the same associations, that is,
  we will show that they have the same graph and thus are the same function.

  Let $c$ be an element of the domain~$C$ of the two.
  The prior item shows that~$f$ is onto so there exists a~$d\in D$ such that
  $f(d)=c$.
  The prior item also shows that~$f$ is one-to-one, so there is only 
  one~$d$ with that property:
  $d=g_0(c)$ and $d=g_1(c)$.
  Therefore $g_0(c)=g_1(c)$.  
\item
  An inverse is a function that has an inverse: the inverse of~$f^{-1}$
  is~$f$.
  By the first item then, $f^{-1}$ is a correspondence.  
\item
  Here is the verification that one order gives the identity function   
  $\composed{(\composed{f^{-1}}{g^{-1}})}{(\composed{g}{f})}\,(x)
  =(\composed{f^{-1}}{g^{-1}})\,(g(f(x)))
  =f^{-1}(g^{-1}(g(f(x))))
  =f^{-1}(f(x))=x$.
  Verification of the other order is similar.  
\end{exparts}
\end{answer}
\end{exercise}


\begin{exercise}   \label{CorrespondingSetsHaveSameNumberOfElements}
  Let $D$ and~$C$ be finite sets. 
  Prove that if there is a correspondence~$\map{f}{D}{C}$
  then the two have the same number of elements.
  \hint for each you can do induction either on $|C|$ or~$|D|$.
\begin{exparts}
\item
  If $f$ is one-to-one then $|C|\geq |D|$.
\item
  If $f$ is onto then $|C|\leq |D|$.
\end{exparts}
\begin{answer}
\begin{exparts}
\item
  We will will prove this by induction on the number of elements in
  the codomain~$C$.
  The base step is that the codomain is empty,~$|C|=0$.
  The only function with an empty codomain is the empty function. 
  The definition of function requires that every element of the domain is 
  mapped somewhere, so the domain must not have any members, $|D|=0$.
  Thus $|C|\geq |D|$.

  For the inductive step assume the statement is true for all one-to-one
  functions between finite sets where the codomains satisfy
  $|C|=0$, \ldots{} $|C|=k$. 
  Consider the next case, $|C|=k+1$.
  Because the codomain does not have zero elements, we can
  fix some~$\hat{c}\in C$.
  Let $\hat{C}=C-\set{\hat{c}}$.
  There are two cases: (1)~there is
  a domain element $d\in D$ with $f(d)=\hat{c}$,  
  and (2)~there isn't any such~$d$.
  
  Case~(2) is easier.
  Consider the restriction map 
  $\map{\hat{f}}{D}{\hat{C}}$ given by $\hat{f}(d)=f(d)$.
  Because $f$ is one-to-one, the map~$\hat{f}$ is also one-to-one.
  The inductive hypothesis give $|\hat{C}|\geq |D|$, and therefore
  $|C|>|\hat{C}|+1\geq |D|$.

  In Case~(1), where there is a domain element~$\hat{d}\in D$
  so that $f(\hat{d})=\hat{c}$, observe that because~$f$ is one-to-one there
  is only one such domain element.
  Let $\hat{D}=D-\set{\hat{d}}$.
  Consider the restriction $\map{\hat{f}}{\hat{D}}{\hat{C}}$ given by 
  $\hat{f}(d)=f(d)$ for $d\in\hat{D}$.
  Because~$f$ is one-to-one, this map is also one-to-one.
  By the inductive hypothesis 
  $|\hat{C}|\geq |\hat{D}|$ and adding~$1$ gives
  $|C|=|\hat{C}|+1\geq |\hat{D}|+1=|D|$.
 
  \smallskip
  \noindent\textit{Alternate proof.}
  We can instead do induction on the number of elements in the domain~$D$.
  The base step is that the domain is empty,~$|D|=0$.
  The only map with an empty domain is the empty function but in any event,
  the codomain cannot have fewer than zero elements
  so~$|C|\geq |D|$.

  For the inductive step assume that the statement is true
  for any one-to-one function between finite sets with $|D|=0$, 
  \ldots{} $|D|=k$. 
  Consider~$|D|=k+1$.
  Fix some $\hat{d}\in D$ and let
  $\hat{D}=D-\set{\hat{d}}$; such an element exists because $|D|=k+1$.
  Observe that because $f$ is one-to-one the image of~$\hat{d}$, 
  $\hat{c}=f(\hat{d})$,
  is not the image of any other element of the domain.
  That is, for $d\neq \hat{d}$ we have $f(d)\neq\hat{c}$.
  Set $\hat{C}=C-\set{\hat{c}}$.
 
  The restriction 
  $\map{\hat{f}}{\hat{D}}{\hat{C}}$ 
  given by $\hat{f}(d)=f(d)$ for~$d\in\hat{D}$
  is one-to-one because $f$ is one-to-one.
  The inductive hypothesis applies so  
  $|\hat{C}|\geq |\hat{D}|$.
  Add~$1$ to get
  $|C|=|\hat{C}|+1\geq |\hat{D}|+1=|D|$.
\item
  The proof will be by induction on the number of elements in the codomain.
  Before we start, here is an illustration.
  Take $D=\set{d_0,d_1,d_2,d_3}$ and $C=\set{c_0,c_1,c_2}$.
  Consider the function $\map{f}{D}{C}$ given here.
  \begin{equation*}
    d_0\mapsto c_0
    \quad d_1\mapsto c_1
    \quad d_2\mapsto c_2
    \quad d_3\mapsto c_2
  \end{equation*}
  That map is onto.
  To make the induction work we pick an element of the codomain
  and omit it; suppose we omit~$c_2$,
  along with all of the the elements of the domain that map to it,
  $f^{-1}(c_2)=\set{d_2,d_3}$.
  We are left with the function $\hat{f}$ with 
  domain
  $\hat{D}=\set{d_0,d_1}$ and codomain~$\hat{C}=\set{c_0,c_1}$ given here.
  \begin{equation*}
    d_0\mapsto c_0
    \quad d_1\mapsto c_1
  \end{equation*}
  This map, the restriction of~$f$ to the set~$\hat{D}$, is onto.
  With that, 
  the inductive hypothesis gives that the restricted 
  domain~$\hat{D}$ has at least 
  as many elements as $\hat{f}$'s codomain $\hat{C}$. 
  Add back the omitted elements to get the function~$f$, and  
  we conclude that its domain~$D$ has at least as many elements as its 
  codomain~$C$.

  Now for the proof.
  The base step is that there are no elements in the codomain, 
  that $C=\emptyset$.
  This implies that the domain is also empty because the definition of function
  requires that every element of the domain is mapped somewhere.
  Hence $|C|\leq |D|$.

  For the inductive step assume that the statement holds for all functions
  between finite sets that are onto some codomain~$C$, 
  where $|C|=0$, $|C|=1$, \ldots{} $|C|=k$.  
  Consider an onto map where $|C|=k+1$.

  The codomain has~$k+1$ elements so it is not empty; fix some~$c\in C$.
  Let $\hat{C}=C-\set{c}$.
  Because~$f$ is onto, the set 
  $f^{-1}(c)=\set{d\in D\suchthat f(d)=c}$ has at least one element,
  that is, $|\hat{D}|+1\leq|D|$.
  Finally, observe that the map
  $\map{\hat{f}}{\hat{D}}{\hat{C}}$
  is onto.
  Therefore the inductive hypothesis gives that 
  $|\hat{D}|\geq |\hat{C}|$.
  Finish by adding one, 
  $|C|=|\hat{C}|+1\leq |\hat{D}|+1\leq |D|$.
 
  \smallskip
  \noindent\textit{Alternate proof.}
  We can instead 
  induction on the number of elements in the domain~$D$.
  As above, before the proof we first give an illustration.
  There are two cases.
  In both cases the domain is $D=\set{d_0,d_1,d_2,d_3}$, and to do induction
  on the number of elements in~$D$ we will omit~$d_3$, and
  consider $\hat{D}=D-\set{d_3}$.
  In the first case the codomain is $C=\set{c_0,c_1,c_2}$ and this is the
  function~$\map{f}{D}{C}$.
  \begin{equation*}
    d_0\mapsto c_0
    \quad d_1\mapsto c_1
    \quad d_2\mapsto c_2
    \quad d_3\mapsto c_2    
  \end{equation*}
  In this case, we consider the map $\map{f}{\hat{D}}{C}$.
  It is onto, so the inductive hypothesis applies and we get that
  $|\hat{D}|\geq|C|$.
  Then we get the desired conclusion $|D|>|\hat{D}|\geq |C|$.

  The second illustrative
  case is that the codomain is $C=\set{c_0,c_1,c_2,c_3}$ with this 
  function~$\map{f}{D}{C}$.
  \begin{equation*}
    d_0\mapsto c_0
    \quad d_1\mapsto c_1
    \quad d_2\mapsto c_2
    \quad d_3\mapsto c_3    
  \end{equation*}
  Since we are omitting~$d_3$ we will also omit~$c_3$, giving
  $\hat{C}=C-\set{c_3}$.
  Consider the restriction map
  $\map{\hat{f}}{\hat{D}}{\hat{C}}$ given by $\hat{f}(d)=f(d)$ for
  $d\in\hat{D}$.
  This map is onto, so the inductive hypothesis applies and we get that
  $|\hat{D}|\geq|\hat{C}|$.
  Then $|D|=|\hat{D}|+1\geq |\hat{C}|+1=|C|$.

  Now for the proof.
  The base step is that~$D$ has no elements at all, $D=\emptyset$.
  The only function with an empty domain is the empty function. 
  Its range is empty and because it is onto, its codomain
  equals its range, so $C=\emptyset$.
  Thus $|C|\leq |D|$ in this case.

  For the inductive step assume that the statement is true for
  onto functions between finite sets for the cases 
  $|D|=0$, $|D|=1$, \ldots{} $|D|=k$. 
  Consider an onto function where
  $|D|=k+1$.
  Because $|D|>0$ we can 
  fix some $\hat{d}\in D$.
  Let $\hat{D}=D-\set{\hat{d}}$.

  There are two cases:~either (1)~the codomain element $\hat{c}=f(\hat{d})$
  is also the 
  image of another domain element, some $d\in\hat{D}$ with $d\neq \hat{d}$, 
  or else (2)~it is not.
  In case~(1) the restriction
  $\map{\hat{f}}{\hat{D}}{C}$ is onto.
  The inductive hypothesis applies, giving that 
  $|C|\leq |\hat{D}|<|D|$.

  Case~(2) is that $\hat{d}$ is the only element of~$D$ associated 
  by~$f$ with the codomain element~$\hat{c}$.
  Let $\hat{C}=C-\set{hat{c}}$.
  Then the the restriction~$\map{\hat{f}}{\hat{D}}{\hat{C}}$
  is onto.
  The inductive hypothesis gives $|\hat{C}|\leq |\hat{D}|$.
  Consequently,
  $|C|=|\hat{C}|+1 \leq |\hat{D}|+1=|D|$.
\end{exparts}
\end{answer}
\end{exercise}


\end{exercises}
\index{functions|)}



\endinput